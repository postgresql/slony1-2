Source: slony1-2
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
 Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 bison,
 debhelper-compat (= 13),
 docbook (>= 4.2),
 docbook-dsssl,
 docbook-utils,
 docbook-xml (>= 4.2),
 docbook2x (>= 0.8.8-4),
 flex,
 ghostscript,
 groff,
 libjpeg-turbo-progs | libjpeg-progs,
 libpq-dev,
 netpbm,
 openjade,
 opensp,
 postgresql-server-dev-all (>= 148~),
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://slony.info/
Vcs-Git: https://salsa.debian.org/postgresql/slony1-2.git
Vcs-Browser: https://salsa.debian.org/postgresql/slony1-2
XS-Testsuite: autopkgtest

Package: slony1-2-bin
Architecture: any
Depends:
 logrotate,
 postgresql-common,
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Recommends:
 libdbd-pg-perl,
 ntp | time-daemon,
 ${module:Recommends},
Suggests:
 libpg-perl,
 slony1-2-doc,
Provides:
 slony1-bin,
Conflicts:
 slony1-bin,
Description: replication system for PostgreSQL: daemon and administration tools
 Slony-I is an asynchronous master-to-multiple-slaves replication system
 for PostgreSQL with cascading and slave promotion.
 .
 This package contains the slon daemon and the slonik administration tool.
 It should be installed on those hosts where Slony-I daemons are to be run
 or administered, which usually, but not necessarily, are the hosts where
 the database server nodes are running.

Package: slony1-2-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Provides:
 slony1-doc,
Conflicts:
 slony1-doc,
Description: Slony-I documentation
 Slony-I is an asynchronous master-to-multiple-slaves replication system
 for PostgreSQL with cascading and slave promotion.
 .
 This package contains the documentation for the Slony-I system.  It is
 not required for normal operation.

Package: postgresql-PGVERSION-slony1-2
Architecture: any
Depends:
 postgresql-PGVERSION,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 slony1-2-bin,
Provides:
 postgresql-PGVERSION-slony1,
Description: replication system for PostgreSQL: PostgreSQL PGVERSION server plug-in
 Slony-I is an asynchronous master-to-multiple-slaves replication system
 for PostgreSQL with cascading and slave promotion.
 .
 This package contains the support functions that are loaded into the
 PostgreSQL database server.  It needs to be installed on the hosts where
 the database server nodes are running.  This package works with version
 PGVERSION of the PostgreSQL server; you need the package that corresponds to
 the version of your database server.
 .
 The actual replication daemon and the administration tools are in the
 package slony1-2-bin.  This package is useless without slony1-2-bin installed
 somewhere in the network.
