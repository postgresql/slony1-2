#!/bin/bash

# This is a simple test to see if the slony1 package is working.  It
# sets up two nodes and replicates a few rows.  If the data arrives on
# the slave, it's OK.
#
# Written by Peter Eisentraut <petere@debian.org>

set -ex

: ${PGVERSION=$(pg_buildext supported-versions . | head -n 1)}

MASTER_PORT=65432
SLAVE_PORT=65433

MASTER_DB=test1
SLAVE_DB=test2

MASTER_CONNINFO="dbname=$MASTER_DB port=$MASTER_PORT user=postgres"
SLAVE_CONNINFO="dbname=$SLAVE_DB port=$SLAVE_PORT user=postgres"


clean() {
    /etc/init.d/slony1 stop
    rm -rf /etc/slony1/{master,slave}/
    pg_dropcluster --stop $PGVERSION master || :
    pg_dropcluster --stop $PGVERSION slave || :
}


setup_slon() {
    local name=$1
    local conn=$2

    mkdir -p /etc/slony1/$name

    zcat /usr/share/doc/slony1-2-bin/examples/slon.conf-sample.gz \
    | sed \
	-e "s/^#cluster_name=.*\$/cluster_name='testcluster'/" \
	-e "s/^#conn_info=.*\$/conn_info='$conn'/" \
	>/etc/slony1/$name/slon.conf
}


if [ x"$1" = x"--clean" ]; then
    clean
    exit 0
fi

/etc/init.d/slony1 stop

pg_dropcluster --stop $PGVERSION master || :
pg_dropcluster --stop $PGVERSION slave || :

pg_createcluster --port $MASTER_PORT --start $PGVERSION master
pg_createcluster --port $SLAVE_PORT --start $PGVERSION slave

su - postgres -c "createdb -p $MASTER_PORT $MASTER_DB"
su - postgres -c "createdb -p $SLAVE_PORT $SLAVE_DB"

case $PGVERSION in
	8.*) su - postgres -c "createlang -p $MASTER_PORT plpgsql $MASTER_DB";;
esac

su - postgres -c "psql -p $MASTER_PORT -d $MASTER_DB -c 'CREATE TABLE testtable (a int PRIMARY KEY, b text);'"
su - postgres -c "pg_dump --cluster $PGVERSION/master -s -p $MASTER_PORT $MASTER_DB | psql -p $SLAVE_PORT $SLAVE_DB"

su - postgres -c slonik <<_EOF_
    cluster name = testcluster;

    node 1 admin conninfo = '$MASTER_CONNINFO';
    node 2 admin conninfo = '$SLAVE_CONNINFO';

    init cluster ( id = 1, comment = 'Node 1' );

    create set ( id = 1, origin = 1, comment = 'All tables' );
    set add table ( set id = 1, origin = 1, id = 1, fully qualified name = 'public.testtable', comment = 'Test table' );

    store node ( id = 2, event node = 1, comment = 'Node 2' );
    store path ( server = 1, client = 2, conninfo = '$MASTER_CONNINFO');
    store path ( server = 2, client = 1, conninfo = '$SLAVE_CONNINFO');
_EOF_


setup_slon master "$MASTER_CONNINFO"
setup_slon slave "$SLAVE_CONNINFO"

/etc/init.d/slony1 start
sleep 2
/etc/init.d/slony1 status

su - postgres -c "psql -p $MASTER_PORT -d $MASTER_DB -c \"INSERT INTO testtable VALUES (1, 'one');\""

su - postgres -c slonik <<_EOF_
    cluster name = testcluster;

    node 1 admin conninfo = '$MASTER_CONNINFO';
    node 2 admin conninfo = '$SLAVE_CONNINFO';

    subscribe set ( id = 1, provider = 1, receiver = 2, forward = no);
_EOF_

su - postgres -c "psql -p $MASTER_PORT -d $MASTER_DB -c \"INSERT INTO testtable VALUES (2, 'zwei');\""

sleep 12


diff -u \
    <(su - postgres -c "psql -p $MASTER_PORT -d $MASTER_DB -c 'SELECT * FROM testtable;'") \
    <(su - postgres -c "psql -p $SLAVE_PORT -d $SLAVE_DB -c 'SELECT * FROM testtable;'")
result=$?

if [ x"$1" != x"--noclean" ]; then
    clean
fi

if [ $result -eq 0 ]; then
    echo "SUCCESS"
else
    echo "FAILED"
fi

exit $result
